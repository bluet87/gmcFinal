#include "ofApp.h"

//NOTE: Implement some noise into this


//--------------------------------------------------------------
void ofApp::setup() {
	ofBackground(0, 0, 0);
	ofSetColor(255, 0, 200);
	ofSetLineWidth(3);
	//ofSetFrameRate(1);
	//ofSetFrameRate(5);
	//ofSetFrameRate(20);
	ofSetFrameRate(10);

	int numLines = ofRandom(3, 5);
	//int numLines = 1;
	for (int i = 0; i < numLines; i++) {
		ofPolyline newLine;
		newLine.addVertex(mouse);
		lines.push_back(newLine);
		lifespan.push_back(maxAlpha);
	}
	ofTranslate(ofGetWidth() / 2, ofGetHeight() / 2, 0);
}

//--------------------------------------------------------------
void ofApp::update() {
	for (int i = 0; i < lines.size(); i++) {
		unsigned int dir = (int)ofRandom(-2.99, 2.99);
		int tempStep = (int)ofRandom(stepMin, stepMax);
		if (dir == -2) {
			stepV = { -tempStep, 0, 0 };
		}
		else if (dir == -1) {
			stepV = { 0, -tempStep, 0 };
		}
		else if (dir == 1) {
			stepV = { tempStep,0,0 };
		}
		else if (dir == 2) {
			stepV = { 0, tempStep, 0 };
		}
		else {
			stepV = { 0,0,0 };
		}
		int lastIndex = lines[i].size() - 1;
		ofPolyline currentLine = lines[i];
		glm::vec3 newPoint = currentLine[lastIndex] + stepV;
		lines[i].addVertex(newPoint);
	}
}

//--------------------------------------------------------------
void ofApp::draw() {


	for (int i = 0; i < lines.size(); i++) {

		//sets colors
		int pinkAlpha = lifespan[i] - 45;
		if (pinkAlpha < 0) {
			pinkAlpha = 0;
		}
		int blueAlpha = lifespan[i];
		if (blueAlpha < 0) {
			blueAlpha = 0;
		}
		int whiteAlpha = lifespan[i] - 100;
		if (whiteAlpha < 0) {
			whiteAlpha = 0;
		}

		//translates the grid to be offset for the pink line
		ofPushMatrix();
		ofTranslate(10, 6);

		//randomly glitch the current line
		ofPolyline glitched = glitchLine(lines[i]);


		// we want to glitch just the altered point, the point before, and the point after

		//if the current line 

		//


		//
		if (!isEqualLine(lines[i], glitched)) {
			cout << "WHITE" << endl;
			//if (0 == 0){
				//sets an Alpha for white

			//set line to pink, draw
			//ofPolyline glitched = glitchLine(lines[i]);
			ofSetColor(255, 0, 200, pinkAlpha);
			glitched.draw();

			//translating for white line
			//ofPushMatrix();
			ofTranslate(3, 3, 0);
			//set line to white
			ofSetColor(255, 255, 255, whiteAlpha);
			glitched.draw();

			//ofPopMatrix();
			//restores to original blue grid
			ofPopMatrix();

			//set line to blue, draw
			ofSetColor(37, 128, 255, blueAlpha);
			glitched.draw();
		}
		/*
		//sideways glitch
		if (ofGetFrameNum() % 120 == 0) {

			//sets an Alpha for white
			int whiteAlpha = lifespan[i] - 80;
			if (whiteAlpha < 0) {
				whiteAlpha = 0;
			}

			//set line to pink
			ofSetColor(255, 0, 200, pinkAlpha);

			//translates grid to the right for the glitch
			ofPushMatrix();
			ofTranslate(glitch);
			lines[i].draw();

			//translating for white line
			//ofPushMatrix();
			ofTranslate(3, 3, 0);
			//set line to white
			ofSetColor(255, 255, 255, whiteAlpha);
			lines[i].draw();

			//ofPopMatrix();

			//goes back to the offset pink grid
			ofPopMatrix();

			//goes back to the original grid, for the blue line
			ofPopMatrix();

			//set line to blue
			ofSetColor(37, 128, 255, blueAlpha);

			//translates grid to the right for the glitch
			ofPushMatrix();
			ofTranslate(glitch);
			lines[i].draw();

			//goes back to original grid
			ofPopMatrix();
		}

		//vertical glitch
		else if (ofGetFrameNum() % 60 == 0) {

			//sets an Alpha for white
			int whiteAlpha = lifespan[i] - 80;
			if (whiteAlpha < 0) {
				whiteAlpha = 0;
			}

			//set line to pink
			ofSetColor(255, 0, 200, pinkAlpha);

			//translates grid to the right for the glitch
			ofPushMatrix();
			ofTranslate(0,50,0);
			lines[i].draw();

			//translating for white line
			//ofPushMatrix();
			ofTranslate(3, 3, 0);
			//set line to white
			ofSetColor(255, 255, 255, whiteAlpha);
			lines[i].draw();

			//ofPopMatrix();
			//goes back to the offset pink grid
			ofPopMatrix();

			//goes back to the original grid, for the blue line
			ofPopMatrix();

			//set line to blue
			ofSetColor(37, 128, 255, blueAlpha);

			//translates grid to the right for the glitch
			ofPushMatrix();
			ofTranslate(0,50,0);
			lines[i].draw();

			//goes back to original grid
			ofPopMatrix();
		}
		*//////////////NOTE THERE SHOULD BE ANOTHER */

		//normal drawing, currently on pink offset grid
		else {
			cout << " " << endl;
			//set line to pink, draw
			ofSetColor(255, 0, 200, pinkAlpha);
			lines[i].draw();

			//restores to original blue grid
			ofPopMatrix();

			//set line to blue, draw
			ofSetColor(37, 128, 255, blueAlpha);
			lines[i].draw();

			//no need for more push/pops
		//}

		//reduces lifespan 
			lifespan[i] -= 2;
			if (pinkAlpha == 0 && blueAlpha == 0) {
				lifespan[i] = 0;
			}

			//if (rotation > .1 || rotation < -.3) {
			//	rotationDirection *= -1;
			//}
			//rotation += rotationDirection;
			//int shake = (int)ofRandom(1, 4);
			//shake *= 5;
			//if (ofGetFrameNum() % shake == 0) {
			//	ofRotate(rotation);
			//}
			//int pinkAlpha = lifespan[i] - 45;
			//if (pinkAlpha < 0) {
			//	pinkAlpha = 0;
			//}
			//ofSetColor(255, 0, 200, pinkAlpha);
			//lines[i].draw();
			//ofPopMatrix();
			//int blueAlpha = lifespan[i];
			//if (blueAlpha < 0) {
			//	blueAlpha = 0;
			//}
			//ofSetColor(37, 128, 255, blueAlpha);
			//lines[i].draw();
			//lifespan[i] -= 2;
			//if (pinkAlpha == 0 && blueAlpha == 0) {
			//	lifespan[i] = 0;
			//}
		}
	}
}


bool ofApp::isEqualLine(ofPolyline original, ofPolyline comparison) {
	bool equal = true;
	for (int i = 0; i < original.size(); i++) {
		if (original[i] != comparison[i]) {
			equal = false;
		}
	}
	return equal;

}

ofPolyline ofApp::glitchLine(ofPolyline line) {
	//goals: take random points from the line and glitch them in random cardinal directions
	//probably tempLine = glitchLine(line[i]); tempLine.draw(); 
	//randomly call glitchline 

	//when glitching, we want to check that the previous two points and the two points afterwards are the same

	ofPolyline modLine = line;

	//currently testing same Glitch same
	//does not pick the last two points
	if (line.size() < 3) {
		return modLine;
	}
	int point = (int)ofRandom(line.size() - 3);
	glm::vec3 wellSee = line[point + 1];
	if ((point - 1) < 1) {
		point = 1;
	}

	if (line[point - 1] == line[point] && line[point] == line[point + 1]) {

		//cout << "GLITCH" << endl;
		glm::vec3 temp = line[point];
		int rand = (int)ofRandom(0, 2);
		if (rand == 0) {
			temp += {ofRandom(-100, 100), 0, 0};
		}
		else if (rand == 1) {
			temp += {0, ofRandom(-100, 100), 0};
		}
		//else if (rand = 2) {
			//temp += {ofRandom(-100, 100), ofRandom(-100, 100), 0};
		//}
		modLine[point] = temp;
		return modLine;
	}
	else {
		//cout << " " << endl;
		return line;
	};

}
//--------------------------------------------------------------
void ofApp::keyPressed(int key) {

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key) {
	if (key == 'c') {
		lines.clear();
		lifespan.clear();
	}
}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y) {
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button) {
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button) {
	//lines.clear();
	mouse = { mouseX, mouseY, 0 };
	int numLines = ofRandom(1, 4);
	for (int i = 0; i < numLines; i++) {
		ofPolyline newLine;
		newLine.addVertex(mouse);
		lines.push_back(newLine);
		lifespan.push_back(maxAlpha);
	}
	rotation = 0;
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button) {

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y) {

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h) {

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg) {

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo) {

}
