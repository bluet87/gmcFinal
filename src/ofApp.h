#pragma once

#include "ofMain.h"

class ofApp : public ofBaseApp {

public:
	void setup();
	void update();
	void draw();

	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void mouseEntered(int x, int y);
	void mouseExited(int x, int y);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);

	ofPolyline line;
	glm::vec3 center{ ofGetWidth() / 2, ofGetHeight() / 2,0 };
	glm::vec3 mouse{ mouseX, mouseY, 0 };
	int stepMin = 20;
	int stepMax = 50;
	glm::vec3 stepV;
	std::vector <ofPolyline> lines;
	vector <int> lifespan;
	int maxAlpha = 255;
	float rotation = 0;
	float rotationDirection = .1;
	glm::vec3 glitch{ 100,0,0 };

	bool isEqualLine(ofPolyline original, ofPolyline comparison);

	ofPolyline glitchLine(ofPolyline line);
};
